FROM ruby:2.7-alpine3.14

# libcurl is for typhoeus gem
RUN apk add --no-cache --update \
    mysql-client \
    mysql-dev \
    build-base \
    less

RUN gem install 'puma' \
  'sinatra' \
  'mysql2' \
  'activerecord' \
  'sinatra-activerecord'

RUN mkdir /app
WORKDIR /app

COPY . /app

EXPOSE 3000

ENTRYPOINT ["ruby", "app.rb"]
