dep-up:
	docker-compose -f docker-compose-dev.yml up -d

dep-down:
	docker-compose -f docker-compose-dev.yml down

dep-down-force:
	docker-compose -f docker-compose-dev.yml down -v

dev-build:
	docker build -t shipout-ssc-dev -f Dockerfile-dev .

dev-console:
	docker run -p 3000:3000 -v $$(pwd):/app -it --rm ruby:2.7-alpine3.14 sh

server:
	ruby app.rb
