# Demo App

開發環境需求
- docker
- docker compose

## 啟動測試環境

啟動 MySQL 資料庫

```
make dep-up
```

啟動 Ruby Container

```
make dev-console
```

於容器中安裝依賴套件

```
apk add --no-cache --update \
    mysql-client \
    mysql-dev \
    build-base \
    less

gem install 'puma' \
  'sinatra' \
  'mysql2' \
  'activerecord' \
  'sinatra-activerecord'
```

於容器中啟動應用程式

```
make server
```

開啟瀏覽器訪問 http://localhost:5000

## 停止測試環境

從容器中退出

執行

```
make dep-down
```

## 建置應用程式容器

```
docker build -t demo .
```

## 啟動應用程式容器

```
docker run -p 3000:3000 demo
```

掛載不同的資料庫設定

```
docker run -p 3000:3000 -v /path/to/database.yml:/app/config/database.yml demo
```
