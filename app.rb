require 'sinatra'
require 'sinatra/activerecord'
require 'erb'

class CreateUsersTable < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.timestamps
    end
  end
end

class User < ActiveRecord::Base
  self.table_name = 'users'
  validates_presence_of :name
  validates_presence_of :email
end

CreateUsersTable.migrate(:up) unless User.table_exists?

TEMPLATE_USERS = <<-TMP
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <title>User List</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mini.css/3.0.1/mini-default.min.css">
  </head>
  <body>
    <div style="text-align: center">
      <h1>Users</h1>
    </div>
    <table>
      <thead>
        <tr>
          <th>Name</th>
          <th>Email</th>
        </tr>
      </thead>
      <tbody>
        <% @users.each do |user| %>
          <tr>
            <td data-label="Name"><%= user.name %></td>
            <td data-label="Email"><%= user.email %></td>
          </tr>
        <% end %>
      </tbody>
    </table>
    <div style="text-align: right">
      <a href="/users/new" class="button">Add User</a>
    </div>
  </body>
</html>    
TMP

TEMPLATE_NEW_USER = <<-TMP
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <title>New User</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mini.css/3.0.1/mini-default.min.css">
  </head>
  <body>
    <div style="text-align: center">
      <h1>New User</h1>
    </div>

    <form action="/users" method="post">
      <fieldset>
        <div class="row" style="align-items: center">
          <div class="col-sm-12 col-md-6" style="text-align: right">
            <label for="name">Name</label>  
          </div>  
          <div class="col-md-3">
            <input type="text" name="user[name]" id="name" value="<%= @user.name %>">
          </div>
        </div>

        <div class="row" style="align-items: center">
          <div class="col-sm-12 col-md-6" style="text-align: right">
            <label for="email">Email</label>
          </div>  
            <div class="col-md-3">
            <input type="email" name="user[email]" id="email" value="<%= @user.email %>">
          </div>
        </div>
      
        <div class="row" style="align-items: center">
          <div class="col-sm-12 col-md-6" style="text-align: right">
            <a href="/" class="button">Cancel</a>
          </div>
          <div class="col-sm-12 col-md-6">
            <input type="submit" value="Create">
          </div>
        </div>
      </fieldset>
    </form>
  </body>
</html>
TMP

set :bind, '0.0.0.0'
set :port, 3000

get '/' do
  @users = User.all
  erb TEMPLATE_USERS
end

post '/users' do
  @user = User.new(params[:user])
  if @user.save!
    redirect '/'
  else
    erb TEMPLATE_NEW_USER
  end
end

get '/users/new' do
  @user = User.new
  erb TEMPLATE_NEW_USER
end
